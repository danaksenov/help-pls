package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Books[] book = new Books[3];

        Books book0 = new Books();
        book0.author = "Лев Толстой";
        book0.nameofbook = "Анна Каренина";
        book0.yearOfPublication = 2010;
        book[0] = book0;

        Books book1 = new Books();
        book1.author = "Фёдор Достоевский";
        book1.nameofbook = "Преступление и наказание";
        book1.yearOfPublication = 1866;
        book[1] = book1;

        Books book2 = new Books();
        book2.author = "Александр Пушкин";
        book2.nameofbook = "Руслан и Людмила";
        book2.yearOfPublication = 1820;
        book[2] = book2;


        int numOfBook = 0;
        int min = book[0].yearOfPublication;
        for (int i = 0; i < 3; i++)
            if (min > book[i].yearOfPublication) {
                min = book[i].yearOfPublication;
                numOfBook = i;
            }
        System.out.println("Автор самой старой книги " + book[numOfBook].author);

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите автора: ");
        String nameOfFoundAuthor = scan.nextLine();
        for (int i = 0; i < 3; i++) {
            if (book[i].author.toLowerCase().contains(nameOfFoundAuthor.toLowerCase())) {
                System.out.println("Книга этого автора: " + book[i].nameofbook);
            }
        }
        System.out.println("Введите год: ");
        int year = scan.nextInt();
        for (Books books : book) {
            if (year > books.yearOfPublication) {
                System.out.println(books.nameofbook + " : " + books.author + " " + books.yearOfPublication);
            } else {
                System.out.println("Книга изданна позже этого года!");
            }


        }

    }
}




